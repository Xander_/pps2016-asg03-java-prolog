package u07asg.app;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * Created by Alessandro on 22/04/2017.
 */
public class TicTacToeBoard extends JComponent{

    private final List<JButton> board;
    private final int w;
    private final int h;

    public TicTacToeBoard(final int w, final int h){
        this.w = w;
        this.h = h;
        board = new ArrayList<>(w*h);
    }

    public JButton get(final int i, final int j){
        return board.get(i*w+j);
    }

    public void set(final int i, final int j, final JButton b){

        board.add(i*w+j, b);
    }

    public void reset(){
        this.board.forEach(b -> {b.setText(""); b.setEnabled(true);});
    }
}
