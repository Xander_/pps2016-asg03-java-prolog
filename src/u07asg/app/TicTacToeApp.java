package u07asg.app;


import u07asg.engine.TicTacToeImpl;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * A not so nicely engineered App to run TTT games, not very important.
 * It just works..
 */
public class TicTacToeApp {

    private static final String N4NTTTEngine = "src/u07asg/ttt_n4n.pl";
    private static final String MisereTTTEngine = "src/u07asg/ttt_misere.pl";

    private static final int DEFAULT_EDGE_LEN = 3;

    public static void main(String[] args) {

        final JFrame gameModeChooser = new JFrame("Choose a Game Mode");
        gameModeChooser.setLayout(new BorderLayout(10, 10));
        gameModeChooser.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        final JPanel north = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 10));
        final JLabel dimLabel = new JLabel("Edge of the board: (Min: 3, Max: 15)");
        north.add(dimLabel);
        final JTextField dimText = new JTextField(Integer.toString(DEFAULT_EDGE_LEN), 3);
        dimText.addKeyListener(new KeyListener() {

            @Override
            public void keyTyped(KeyEvent e) {
                if(dimText.getText().length() >= dimText.getColumns()-1 &&
                        (!(e.getKeyChar() == KeyEvent.VK_DELETE ||
                                e.getKeyChar() == KeyEvent.VK_BACK_SPACE))) {
                    e.consume();
                }
            }

            @Override
            public void keyPressed(KeyEvent e) {}

            @Override
            public void keyReleased(KeyEvent e) {}
        });
        north.add(dimText);

        final JPanel center = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 10));

        final JButton misereGameMode = new JButton("Misère");
        final JButton nInARowGameMode = new JButton("N-In-A-Row");

        nInARowGameMode.addActionListener(e ->
                openGameBoard("N-In-A-Row", N4NTTTEngine, getDim(dimText), getDim(dimText)));
        misereGameMode.addActionListener(e ->
                openGameBoard("Misère", MisereTTTEngine, getDim(dimText), getDim(dimText)));
        center.add(nInARowGameMode);
        center.add(misereGameMode);

        final JPanel south = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 10));
        final JButton exit = new JButton("Exit");
        exit.addActionListener(e -> System.exit(1));
        south.add(exit);

        gameModeChooser.add(center, BorderLayout.CENTER);
        gameModeChooser.add(north, BorderLayout.NORTH);
        gameModeChooser.add(south, BorderLayout.SOUTH);

        gameModeChooser.setSize(350, 200);
        gameModeChooser.setResizable(false);
        gameModeChooser.setVisible(true);
    }

    private static int getDim(final JTextField text){
        int dim =  DEFAULT_EDGE_LEN;

        try{
            dim = Math.max(Integer.parseInt(text.getText()), 3);
            dim = Math.min(dim, 15);
        } catch (Exception ex){
            //ex.printStackTrace();
        }

        return dim;
    }

    private static void openGameBoard(final String title, final String engine, final int w, final int h){
        try {
            new TicTacToeView(title, new TicTacToeController(new TicTacToeImpl(engine, w, h)));
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Problems loading the theory");
        }
    }
}
