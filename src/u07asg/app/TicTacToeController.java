package u07asg.app;

import u07asg.engine.TicTacToe;

import java.util.*;

import static u07asg.app.Player.*;
import static u07asg.app.Player.PlayerSignature.*;
import static u07asg.app.UpdateAction.*;

/**
 *
 * Created by Alessandro on 22/04/2017.
 */
public class TicTacToeController extends Observable {

    private final TicTacToe ttt;
    private final List<Observer> observers;

    private final Map<PlayerSignature, Player> players = new HashMap<>();

    private PlayerSignature turn = PlayerX;
    private boolean finished = false;

//    private int moves = 0;

    public TicTacToeController(TicTacToe ttt) {
        this.ttt = ttt;
        this.ttt.createBoard();
        this.observers = new ArrayList<>();

        players.put(PlayerX, new Player(PlayerX));
        players.put(PlayerO, new Player(PlayerO));

        INFO_LABEL.messageIs("Turn of " + turn.signature());
        MATCH_LABEL.messageIs("[X:" + 0 + " - O:" + 0 + "]");
        NEW_GAME.messageIs("false");
    }

    public int boardWidth(){
        return ttt.boardWidth();
    }

    public int boardHeight(){
        return ttt.boardHeight();
    }

    public boolean isGameFinished(){
        return this.finished;
    }

    private void changeTurn() {
        this.turn = ttt.switcheroo(this.turn);
        INFO_LABEL.messageIs("Turn of " + turn.signature());
        this.notifyObservers(INFO_LABEL);
    }

    public void move(int i, int j) {

        if (ttt.move(turn, i, j)) {

            // E' bello sapere che Java SE 8 non supporta nativamente il format json ...
            UPDATE_BOARD.messageIs(turn.signature() + ":" + i + ":" +j);
            this.notifyObservers(UPDATE_BOARD);

            this.changeTurn();

//            moves++;
//
//            if (moves > this.boardWidth() && this.boardWidth() <= 3) {
//
//                final int posWinsX = ttt.waysToWin(turn, PlayerX);
//                final int posWinsO = ttt.waysToWin(turn, PlayerO);
//
//                final double winChancesX = Math.round(100.0 / (posWinsX+posWinsO) * posWinsX);
//                final double winChancesO = Math.round(100.0 - winChancesX);
//
//                System.out.println("X can win in " + posWinsX + " possible ways [" + winChancesX + "%].");
//                System.out.println("O can win in " + posWinsO + " possible ways [" + winChancesO + "%].");
//            }
        }

        Optional<PlayerSignature> victory = ttt.checkVictory();

        if (victory.isPresent()) {

            System.out.println(victory.get().signature() + " has won in " + ttt.movesCount(victory.get()) + " moves.");

            players.get(victory.get()).scoreVictory();

            this.notifyObservers(INFO_LABEL.messageIs(ttt.result(victory.get())));

            this.notifyObservers(MATCH_LABEL
                    .messageIs("[X:"+ players.get(PlayerX).victories() +" - O:"+ players.get(PlayerO).victories()+"]"));

            this.notifyObservers(NEW_GAME.messageIs("true"));

            finished = true;

            // Palla ai polli
            turn = ttt.switcheroo(victory.get());

        } else if (ttt.checkEven()) {

            this.notifyObservers(INFO_LABEL.messageIs(ttt.result(null)));
            finished = true;
            //return;
        }
    }

    public void newGame(){
        this.notifyObservers(INFO_LABEL.messageIs("Turn of " + turn.signature()));

        finished = false;
//        moves = 0;

        this.ttt.createBoard();

        this.notifyObservers(NEW_GAME.messageIs("false"));

        this.notifyObservers(NEW_BOARD.messageIs(null));
    }

    @Override
    public void addObserver(Observer observer){
        this.observers.add(observer);
    }

    @Override
    public void notifyObservers(){
        this.notifyObservers(null);
    }

    @Override
    public void notifyObservers(Object obj){
        this.observers.forEach(o -> o.update(this, obj));
    }
}
