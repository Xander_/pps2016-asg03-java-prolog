package u07asg.app;

import javax.swing.*;
import java.awt.*;
import java.util.Observable;
import java.util.Observer;

/**
 *
 * Created by Alessandro on 22/04/2017.
 */
public class TicTacToeView extends JFrame implements Observer{

    private final TicTacToeController tttCtrl;

    private final TicTacToeBoard board;

    private final JButton exit = new JButton("Exit");
    private final JButton newGame = new JButton("New Game");
    private final JLabel infoLabel;
    private final JLabel matchLabel;

    public TicTacToeView(String title, TicTacToeController tttCtrl) throws Exception {
        this.tttCtrl = tttCtrl;
        this.tttCtrl.addObserver(this);

        this.setTitle(title);
        this.setLayout(new BorderLayout());
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        this.board = new TicTacToeBoard(tttCtrl.boardWidth(), tttCtrl.boardHeight());

        infoLabel = new JLabel("");
        UpdateAction.INFO_LABEL.accept(infoLabel);
        matchLabel = new JLabel("");
        UpdateAction.MATCH_LABEL.accept(matchLabel);

        initPane();
    }

    private void initPane() {

        final JPanel boardPanel = new JPanel(new GridLayout(tttCtrl.boardWidth(), tttCtrl.boardHeight()));

        for (int i = 0; i < tttCtrl.boardWidth(); i++) {
            for (int j = 0; j < tttCtrl.boardHeight(); j++) {

                final int i2 = i;
                final int j2 = j;

                board.set(i, j, new JButton(""));
                board.get(i,j).addActionListener(e -> {
                    if (!tttCtrl.isGameFinished()) tttCtrl.move(i2, j2);
                });

                boardPanel.add(board.get(i, j));
            }
        }

        newGame.setEnabled(false);
        newGame.addActionListener(e -> tttCtrl.newGame());

        exit.addActionListener(e -> this.dispose());

        final JPanel southPanel = new JPanel(new FlowLayout());
        southPanel.add(exit);
        southPanel.add(newGame);

        final JPanel northPanel = new JPanel(new FlowLayout());

        northPanel.add(infoLabel);
        northPanel.add(matchLabel);

        this.add(BorderLayout.NORTH, northPanel);
        this.add(BorderLayout.CENTER, boardPanel);
        this.add(BorderLayout.SOUTH, southPanel);

        this.setSize(400, 460);
        this.setVisible(true);
        this.setResizable(true);
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof UpdateAction){
            if(arg.equals(UpdateAction.INFO_LABEL)){
                ((UpdateAction) arg).accept(infoLabel);
            } else if (arg.equals(UpdateAction.MATCH_LABEL)){
                ((UpdateAction) arg).accept(matchLabel);
            } else if (arg.equals(UpdateAction.NEW_GAME)){
                ((UpdateAction) arg).accept(newGame);
            } else if (arg.equals(UpdateAction.UPDATE_BOARD) || arg.equals(UpdateAction.NEW_BOARD)){
                ((UpdateAction) arg).accept(board);
            }
        }

        this.repaint();
    }
}
