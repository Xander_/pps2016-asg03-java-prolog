package u07asg.app;

import javax.swing.*;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 *
 * Created by Alessandro on 22/04/2017.
 */
public enum UpdateAction implements Consumer<JComponent> {

    INFO_LABEL((jc, msg) -> {
        if (jc instanceof JLabel){
            ((JLabel) jc).setText(msg);
        }
    }),
    MATCH_LABEL((jc, msg) -> {
        if (jc instanceof JLabel){
            ((JLabel) jc).setText(msg);
        }
    }),
    NEW_GAME((jc, msg) -> {
        if(jc instanceof JButton){
            jc.setEnabled(Boolean.parseBoolean(msg));
        }
    }),
    UPDATE_BOARD((jc, msg) -> {
        if (jc instanceof TicTacToeBoard){
            final String player =  msg.split(":")[0];
            final int x = Integer.parseInt(msg.split(":")[1]);
            final int y = Integer.parseInt(msg.split(":")[2]);
            ((TicTacToeBoard) jc).get(x, y).setText(player);
            ((TicTacToeBoard) jc).get(x, y).setEnabled(false);
        }
    }),
    NEW_BOARD((jc, msg) -> {
        if (jc instanceof TicTacToeBoard){
            ((TicTacToeBoard) jc).reset();
        }
    });

    private BiConsumer<JComponent, String> updateAction;
    private String message = "";

    UpdateAction(final BiConsumer<JComponent, String> updateAction){
        this.updateAction = updateAction;
    }

    @Override
    public void accept(JComponent jc){
        this.updateAction.accept(jc, this.message);
    }

    public UpdateAction messageIs(String message) {
        this.message = message;
        return this;
    }
}
