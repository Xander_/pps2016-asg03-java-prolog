package u07asg.app;

public class Player {

    private final PlayerSignature signature;

    private int victories = 0;

    Player(PlayerSignature signature) {
        this.signature = signature;
    }

    public int victories(){
        return this.victories;
    }

    public void scoreVictory(){
        this.victories++;
    }

    public String signature() {
        return signature.signature();
    }

    public enum PlayerSignature{
        PlayerX("X"), PlayerO("O");

        private final String symbol;

        PlayerSignature(String symbol) {
            this.symbol = symbol;
        }

        public String signature() {
            return symbol;
        }
    }
}

