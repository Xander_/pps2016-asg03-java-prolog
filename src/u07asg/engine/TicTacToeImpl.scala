package u07asg.engine

import java.io.FileInputStream
import java.util
import java.util.Optional

import alice._
import alice.tuprolog.{Struct, Theory}
import u07asg.app.Player.PlayerSignature
import u07asg.app.Player.PlayerSignature._
import u07asg.engine.Scala2P._

import scala.collection.JavaConverters._
import scala.collection.mutable

/**
  * Created by mirko on 4/10/17.
  */
class TicTacToeImpl(fileName: String, width:Int, height:Int) extends TicTacToe {
    
    implicit private def playerToString(player: PlayerSignature): String = player match {
        case PlayerX => "p1"
        case _ => "p2"
    }
    
    implicit private def stringToPlayer(s: String): PlayerSignature = s match {
        case "p1" => PlayerX
        case _ => PlayerO
    }
    
    private val engine = mkPrologEngine(new Theory(new FileInputStream(fileName)))
    
    override def toString: String = solveOneAndGetTerm(engine, "board(B)", "B").toString
    
    // retractall => free memeory from previous boards, assert => commit changes
    override def createBoard(): Unit =
        solveWithSuccess(engine, s"retractall(board(_)), create_board(B, $width, $height), assert(board(B))")
    
    override def boardWidth: Int =
        solveOneAndGetTerm(engine, s"board(B), dim(B, W, _)", "W").asInstanceOf[tuprolog.Long].intValue()
    
    override def boardHeight: Int =
        solveOneAndGetTerm(engine, s"board(B), dim(B, _, H)", "H").asInstanceOf[tuprolog.Long].intValue()
    
    override def getBoard: util.List[Optional[PlayerSignature]] = {
        val term = solveOneAndGetTerm(engine, "board(B)", "B").asInstanceOf[Struct]
        val iterator = term.listIterator()
        iterator.asScala.toList.map(_.toString).map {
            case "null" => Optional.empty[PlayerSignature]()
            case s => Optional.of[PlayerSignature](s)
        }.to[mutable.Buffer].asJava
    }
    
    override def checkEven(): Boolean = solveWithSuccess(engine, "board(B), isFinal(B, even)")
    
    override def checkVictory(): Optional[PlayerSignature] = {
        
        if (solveWithSuccess(engine, "board(B), isFinal(B, p1)")) Optional.of(PlayerX)
        else if (solveWithSuccess(engine, "board(B), isFinal(B, p2)")) Optional.of(PlayerO)
        else Optional.empty()
    }
    
    override def switcheroo(actual: PlayerSignature): PlayerSignature =
        stringToPlayer(solveOneAndGetTerm(engine, s"other_player(${playerToString(actual)}, Other)", "Other")
            .asInstanceOf[Struct].toString)
    
    override def move(player: PlayerSignature, i: Int, j: Int): Boolean = {
        
        val nextboard = (for {
            term <- engine(s"board(B), next_board(B, ${playerToString(player)}, B2)")
                .map(extractTerm(_, "B2"))
            elem = term.asInstanceOf[Struct].listIterator().asScala.toList(i + width * j)
            if elem.toString == playerToString(player)
        } yield term).headOption
        
        if (nextboard isEmpty) return false
        
        solveWithSuccess(engine, s"retractall(board(_)), assert(board(${nextboard.get.toString}))")
    }
    
    override def waysToWin(current: PlayerSignature, winner: PlayerSignature): Int = {
        val goal = s"board(B), countWaysToWin(B, ${playerToString(current)}, ${playerToString(winner)}, Count)"
        solveOneAndGetTerm(engine, goal, "Count").asInstanceOf[tuprolog.Int].intValue()
    }
    
    override def movesCount(player: PlayerSignature): Int = {
        val goal = s"board(B), countMoves(B, ${playerToString(player)}, Count)"
        solveOneAndGetTerm(engine, goal, "Count").asInstanceOf[tuprolog.Int].intValue()
    }
    
    //override def movesToWin(player: PlayerSignature): Int = 0
    
    override def result(player: PlayerSignature): String ={
        val goal = s"board(B), result(B, ${playerToString(player)}, Message)"
        solveOneAndGetTerm(engine, goal , "Message").asInstanceOf[Struct].toString
    }
}
