package u07asg.engine;

import u07asg.app.Player.PlayerSignature;
import java.util.List;
import java.util.Optional;

public interface TicTacToe {

    void createBoard();

    List<Optional<PlayerSignature>> getBoard();

    int boardWidth();

    int boardHeight();

    boolean checkEven();

    Optional<PlayerSignature> checkVictory();

    PlayerSignature switcheroo(PlayerSignature player);

    boolean move(PlayerSignature player, int i, int j);

    String result(PlayerSignature player);

    int waysToWin(PlayerSignature current, PlayerSignature winner);

    int movesCount(PlayerSignature player);

    //int movesToWin(PlayerSignature player);
}
