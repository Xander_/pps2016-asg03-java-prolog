% Misère Tic Tac Toe => X/O win if O/X should have won instead,
% that means that the victory goes to the player that does not score n-in-a-row symbols.
%
% symbol(+OnBoard, -OnScreen): symbols for the board and their rendering
%
symbol(null, '_').
symbol(p1, 'X').
symbol(p2, 'O').

% dim(-width, -height): specifies which are the width and the height of the board
%
dim(B, W, H) :- board(B), length(B, Dim), W is round(sqrt(Dim)), H is W.

% create_board(-Board): creates an initially empty board
% create_list(+Size, +Element, +Board, -List): create a list representing the new board
%
create_board(B, W, H) :- Size is W*H, create_list(Size, null, B). % 
create_list(0, _, []) :- !.
create_list(N, X, [X|T]) :- N2 is N-1, create_list(N2, X, T).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Some Statistics on the match
%

% countWaysToWin(+Board, +Player, +Result, -Count): counts how many time Res will happen
% BROKEN
%
countWaysToWin(B, P, Res, Count) :- findall(a, game(B, P, _, Res), L), length(L, Count).

% game(+Board, +Player, -FinalBoard, -Result): finds one (zero, one or many) final boards and results
%
game(B, _, B, Result) :- isFinal(B, Result), !.
game(B, PL, BF, Result):- next_board(B, PL, B2), other_player(PL, PL2), game(B2, PL2, BF, Result).

% next_board(+Board, +Player, ?NewBoard): finds (zero, one or many) new boards as Player moves
%
next_board([null | B], PL, [PL | B]).
next_board([X | B], PL, [X | B2]) :- next_board(B, PL, B2).

% other_player(?Player, ?OtherPlayer): switches between the 2 players
% MEMO: lower cases things = Constants
%
other_player(p1, p2).
other_player(p2, p1).

% countMoves(+Board, +Player, -CountMoves): counts how many moves have been done by a player
%
countMoves(B, Pl, CMov) :- occurrences(B, Pl, CMov).

% occurrences(+List, +Elem, -Occurences): counts how many occurrences of Elem are in List
%
% got this from the Slides p.58  @{http://campus.unibo.it/278354/1/07-prolog.pdf}
%
occurrences(L, E, Occ) :- occurrences(L, E, 0, Occ).
occurrences([], _, Occ, Occ).
occurrences([El | T], El, SuppOcc, Occ) :- !, TempOcc is SuppOcc+1, occurrences(T, El, TempOcc, Occ).
occurrences([_ | T], El, SuppOcc, Occ) :- occurrences(T, El, SuppOcc, Occ).

% countMovesToWin(+Board, +Player, -MovesToWin): Count how many moves +Player need to win the Game.
%
%countMovesToWin(B, Pl, MTW).

% result(+OnBoard, -OnScreen): result of a game
%
result(B, p1, "X has won!") :- isFinal(B, p1).
result(B, p1, "X has lost!") :- isFinal(B, p2).
result(B, p2, "O has won!") :- isFinal(B, p2).
result(B, p2, "O has lost!") :- isFinal(B, p1).
result(B, _, "Even!") :- isFinal(B, even).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% isFinal(+Board, -Result): checks where the board is final and why
%
% isFinal for 'X'
isFinal(B, p1) :- other_player(p1, Pl), dim(B, Dim, _), check(B, Dim, Pl), !.
% isFinal for 'O'
isFinal(B, p2) :- other_player(p2, Pl), dim(B, Dim, _), check(B, Dim, Pl), !.
% is a Draw ?
isFinal(B, even) :- not(member(null,  B)).

% checkRows(+Board, +Player): matches if the board contains either "X" or "O" into N squares in a row.
%
check(B, Dim, Pl) :- I is 0, checkNthRow(B, I, Dim, Pl).

% check(+Board, +RowIndex, +Player): matches if at least one of the rows of +Board
% matches its content with the signature of Player
%
checkNthRow(B, I, Dim, Pl) :-
	I >= Dim -> not(true);
	getRow(B, I, Dim, Row), matchContent(Row, Pl) -> !;
    NEXT is I+1, checkNthRow(B, NEXT, Dim, Pl).

% check(+Board, +Player): matches if the board contains either "X" or "O" into N squares in a column.
%
check(B, Dim, Pl) :- J is 0, checkNthCol(B, J, Dim, Pl).

% checkNthCol(+Board, +ColumnIndex, +Player): matches if at least one of the columns of +Board
% matches its content with the signature of Player
%
checkNthCol(B, J, Dim, Pl) :-
	J >= Dim -> not(true);
	getColumn(B, J, Dim, Col), matchContent(Col, Pl) -> !;
		NEXT is J+1, checkNthCol(B, NEXT, Dim, Pl).

% check(+Board, +Player): matches if the board contains either "X" or "O" into N squares in diagonal.
%
check(B, Dim, Pl) :- checkNthDiagonal(B, Dim, 0, Pl).
check(B, Dim, Pl) :- checkNthDiagonal(B, Dim, -1, Pl).

checkNthDiagonal(B, Dim, N, Pl) :-  getDiagonal(B, N, Dim, D), matchContent(D, Pl).

% matchContent(+Row, +Player): succeeds if the content of +Row is made of only +Player elements.
%
matchContent([], Pl).
matchContent([Pl | Rest], Pl) :- matchContent(Rest, Pl).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% getRow(+Board, +RowIndex, -RowElems): return the -RowElems of the +Board found at the wanted +RowIndex
%
getRow(B, I, Dim, R) :-
    FROM is I*Dim,
    TO is Dim+FROM-1,
    sublist(B, R, FROM, TO).

% getColumn(+Board, +ColIndex, -ColElems): return the ColElems of the Board found at the wanted ColIndex
%
getColumn(B, J, Dim, C) :-
	collectColumnElements(B, J, Dim, CS),
	reverse(CS, [N | T]),   % Reverse to get last element as the head
	append(T, [], Temp), 	% Remove head (ex last elem) which is always null, but I dunno why
    reverse(Temp, C).		% Reverse again to get a clean Column

collectColumnElements(B, J, Dim, [El | CS]) :-
	nth(B, J, El),
	J < Dim*Dim ->
	    NEXT is J+Dim, collectColumnElements(B, NEXT, Dim, CS);
	    empty([El | CS]).

% empty(+EmptyMember): return an +EmptyMember (n.d.r.: null) list.
%
empty([null | []]).

% getDiagonal(+Board, +DiagIndex, -DiagElems): return the diagonal of the given board.
% +DiagIndex refers to which diagonal is desired: Main Diagonal or Anti Diagonal.
%
getDiagonal(B, 0, Dim, M) :-
	J is 0, collectMainDiagonalElements(B, J, Dim, MS),
	reverse(MS, [N | T]),
	append(T, [], Temp),
	reverse(Temp, M).

getDiagonal(B, -1, Dim, A) :-
		J is Dim-1, collectAntiDiagonalElements(B, J, Dim, AS),
		reverse(AS, [N | T]),
		append(T, [], Temp),
		reverse(Temp, A).

collectMainDiagonalElements(B, J, Dim, [El | MS]) :-
	nth(B, J, El),
    J < Dim*Dim ->
        NEXT is J+Dim+1, collectMainDiagonalElements(B, NEXT, Dim, MS);
        empty([El | MS]).

collectAntiDiagonalElements(B, J, Dim, [El | AS]) :-
	nth(B, J, El),
    J < Dim*Dim-1 ->
        NEXT is J+Dim-1, collectAntiDiagonalElements(B, NEXT, Dim, AS);
        empty([El | AS]).

% copy(+List, -Copy): Copies the content of +List into -Copy.
%
copy(L, R) :- makeCopy(L, R).
makeCopy([],[]).
makeCopy([H | T1],[H | T2]) :- makeCopy(T1, T2).

% nth(+List, ?Index, ?Element): Return the Element/Index given the Index/Element.
%
% Adapted from SWI-Prolog nth0/3
%
nth(L, I, E) :- nth(L, 0, I, E) .
nth([E | L], I, I, E) :- !.
nth([_ | L], T, I, E) :- T1 is T+1 , nth(L, T1, I, E).

% sublist(+List, -SubList, +FromIndex, +ToIndex): return the sublist found between the given indexes.
%
% StackOverflow <3 @{http://stackoverflow.com/questions/16431498/prolog-extract-a-sublist}
%
sublist(L, Sub, I, J):-
    sublist(L, Temp, I, J, []),
    !,
    reverse(Temp, Sub).

sublist([], Sub, _, _, Sub).

sublist(_, Sub, I, J, Sub):-
    I > J.

sublist(L, Sub, I, J, Sub):-
    I < 0,
    sublist(L, Sub, 0, J, Sub).

sublist([_ | T], Sub, I, J, Acc):-
    I > 0,
    sublist(T, Sub, I-1, J-1, Acc).

sublist([H | T], Sub, I, J, Acc):-
    sublist(T, Sub, I, J-1, [H | Acc]).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% render(+List): prints a TTT table (N elements list) on console
%
render(L) :- convert_symbols(L, R), printListAsMatrix(R).

convert_symbols(L, R) :- findall(S, (member(Pl, L), symbol(Pl, S)), R).

printListAsMatrix(R) :- printRows(R, 0), !.

printRows(Mat, I) :- getRow(Mat, I, Row), print_row(Row), dim(B, Dim, _), NEXT is I+1, NEXT < Dim, printRows(Mat, NEXT).

print_row([]) :- nl.
print_row([El | Rest]) :- put(El), put('	'), print_row(Rest).

printList([]):- nl, !.
printList([El | Rest]) :- print(El), print('	'), printList(Rest).

% render(+List, +Result): prints a TTT table plus result
render_full(L, Result) :- result(Result, OnScreen), print(OnScreen), nl, render(L), nl, nl.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%