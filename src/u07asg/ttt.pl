% symbol(+OnBoard, -OnScreen): symbols for the board and their rendering
symbol(null, '_').
symbol(p1, 'X').
symbol(p2, 'O').

% result(+OnBoard, -OnScreen): result of a game
result(even, "even!").
result(p1, "player 1 wins").
result(p2, "player 2 wins").

% other_player(?Player, ?OtherPlayer)
other_player(p1,p2).
other_player(p2,p1).

% render(+List): prints a TTT table (9 elements list) on console
render(L) :- convert_symbols(L,[A,B,C,D,E,F,G,H,I]), print_row(A,B,C), print_row(D,E,F), print_row(G,H,I).
convert_symbols(L,L2) :- findall(R, (member(X, L), symbol(X, R)), L2).
print_row(A, B, C) :- put(A), put(' '), put(B), put(' '), put(C), nl.

% render(+List, +Result): prints a TTT table plus result
render_full(L, Result) :- result(Result, OnScreen),print(OnScreen), nl, render(L), nl, nl.

% create_board(-Board): creates an initially empty board
create_board(B) :- create_list(9, null, B).
% create_list(+Size, +Element, +Board, -List): create a list representing the new board
create_list(0, _, []) :- !.
create_list(N, X, [X|T]) :- N2 is N-1, create_list(N2, X, T).

% next_board(+Board, +Player, ?NewBoard): finds (zero, one or many) new boards as Player moves
next_board([null|B], PL, [PL|B]).
next_board([X|B], PL, [X|B2]) :- next_board(B, PL, B2).

% final(+Board, -Result): checks where the board is final and why
final(B, p1) :- isAWinningPattern(P), match(B, P, p1),!.
final(B, p2) :- isAWinningPattern(P), match(B, P, p2),!.
final(B, even) :- not(member(null,  B)).

% match(Board,Pattern,Player): checks if in the board, the player matches a winning pattern
match([], [], _).
match([M|B], [x|P], M) :- match(B, P, M).
match([_|B], [o|P], M) :- match(B, P, M).

% isAWinningPattern(+Pattern): gives a winning pattern
isAWinningPattern([x,x,x,o,o,o,o,o,o]).
isAWinningPattern([o,o,o,x,x,x,o,o,o]).
isAWinningPattern([o,o,o,o,o,o,x,x,x]).
isAWinningPattern([x,o,o,x,o,o,x,o,o]).
isAWinningPattern([o,x,o,o,x,o,o,x,o]).
isAWinningPattern([o,o,x,o,o,x,o,o,x]).
isAWinningPattern([x,o,o,o,x,o,o,o,x]).
isAWinningPattern([o,o,x,o,x,o,x,o,o]).

% game(+Board, +Player, -FinalBoard, -Result): finds one (zero, one or many) final boards and results
game(B, _, B, Result) :- final(B, Result),!.
game(B, PL, BF, Result):- next_board(B, PL, B2), other_player(PL, PL2),game(B2, PL2, BF, Result).


% statistics(+Board, +Player, +Result, -Count): counts how many time Res will happen
statistics(B, P, Res, Count) :- findall(a, game(B, P, _, Res), L), length(L, Count).